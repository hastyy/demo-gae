package pt.unl.fct.di.apdc.demo.resources;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;

import pt.unl.fct.di.apdc.demo.util.AuthToken;

@Path("/logout")
@Consumes(MediaType.APPLICATION_JSON)
public class LogoutResource {
	
	// Logger object
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	
	// Datastore link
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	@POST
	public Response doLogout(AuthToken data) {
		LOG.info("Attempting to remove token from database...");
		
		Transaction txn = datastore.beginTransaction();
		Key parent = KeyFactory.createKey("User", data.user);
		Key tokenKey = KeyFactory.createKey(parent, "Token", data.tokenID);
		
		try {
			@SuppressWarnings("unused")
			Entity token = datastore.get(tokenKey);	// If the entity does not exist an Exception is thrown. Otherwise,
		
			datastore.delete(txn, tokenKey);
			txn.commit();
			
			LOG.info("Token deleted successfully!");
			return Response.ok().build();
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Failed logout attempt for user: " + data.user + "with token: " + data.tokenID);
			txn.rollback();
			return Response.status(Status.NOT_FOUND).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
	}
	
}

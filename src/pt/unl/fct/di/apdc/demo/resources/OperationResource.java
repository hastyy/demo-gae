package pt.unl.fct.di.apdc.demo.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import pt.unl.fct.di.apdc.demo.util.Address;
import pt.unl.fct.di.apdc.demo.util.AuthToken;
import pt.unl.fct.di.apdc.demo.util.ReportData;

@Path("/operation")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON)
public class OperationResource {
	
	// Logger object
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	
	// Converts objects to JSON
	private final Gson g = new Gson();
	
	// Datastore link
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public OperationResource() { }	// Empty constructor for code 'correctness'
	
	@POST
	@Path("/{email}")
	public Response getAddress(@PathParam("email") String email, AuthToken token) {
		LOG.info("Attempting to retrieve user's address: " + email);
		
		Key tokenParent = KeyFactory.createKey("User", token.user);
		Key tokenKey = KeyFactory.createKey(tokenParent, "Token", token.tokenID);
		
		try {
			@SuppressWarnings("unused")
			Entity tokenEntity = datastore.get(tokenKey);	// If the entity does not exist an Exception is thrown. Otherwise,
			if (token.expirationData <= System.currentTimeMillis()) {
				LOG.warning("The received token has already expired!");
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (EntityNotFoundException e1) {
			LOG.warning("The received token has no match in the database!");
			return Response.status(Status.FORBIDDEN).build();
		}
		
		Key userKey = KeyFactory.createKey("User", email);
		
		try {
			Entity user = datastore.get(userKey);	// If the entity does not exist an Exception is thrown. Otherwise,
			EmbeddedEntity adr = (EmbeddedEntity) user.getProperty("address");
			Address address = new Address(
					(String) adr.getProperty("street"),
					(String) adr.getProperty("complementary"),
					(String) adr.getProperty("city"),
					(String) adr.getProperty("cp")
			);
			
			LOG.info("Address successfully retrieved: " + email);
			return Response.ok(g.toJson(address)).build();
		} catch (EntityNotFoundException e) {
			LOG.warning("Failed retrieve user's address: " + email);
			return Response.status(Status.NOT_FOUND).build();
		}
	}
	
	@POST
	@Path("/all")
	public Response getAllAdresses(AuthToken token) {
		LOG.info("Attempting to fetch all adresses from the database....");
		
		Key tokenParent = KeyFactory.createKey("User", token.user);
		Key tokenKey = KeyFactory.createKey(tokenParent, "Token", token.tokenID);
		
		try {
			@SuppressWarnings("unused")
			Entity tokenEntity = datastore.get(tokenKey);	// If the entity does not exist an Exception is thrown. Otherwise,
			if (token.expirationData <= System.currentTimeMillis()) {
				LOG.warning("The received token has already expired!");
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (EntityNotFoundException e1) {
			LOG.warning("The received token has no match in the database!");
			return Response.status(Status.FORBIDDEN).build();
		}
		
		Query q = new Query("User");
		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
		
		if (!results.isEmpty()) {
			List<Address> addresses = new ArrayList<Address>(results.size());
			for (Entity e : results) {
				EmbeddedEntity adr = (EmbeddedEntity) e.getProperty("address");
				addresses.add(new Address(
						(String) adr.getProperty("street"),
						(String) adr.getProperty("complementary"),
						(String) adr.getProperty("city"),
						(String) adr.getProperty("cp")
				));
			}
			
			LOG.info("Successfully got all addresses.");
			return Response.ok(g.toJson(addresses)).build();
		} else {
			LOG.warning("Didn't get any addresses.");
			return Response.status(Status.NOT_FOUND).build();
		}
		
	}
	
//	@POST
//	@Path("/registerreport")
//	public Response registerOccurence(String json) {
//		JsonElement jelement = new JsonParser().parse(json);
//		JsonObject  jobject = jelement.getAsJsonObject();
//		JsonObject jreport = jobject.getAsJsonObject("report");
//		JsonObject jtoken = jobject.getAsJsonObject("token");
//	    ReportData report = g.fromJson(jreport.toString(), ReportData.class);
//	    AuthToken token = g.fromJson(jtoken.toString(), AuthToken.class);
//		
//		LOG.info("Attempting to register report...");
//		Key tokenParent = KeyFactory.createKey("User", token.user);
//		Key tokenKey = KeyFactory.createKey(tokenParent, "Token", token.tokenID);
//		try {
//			@SuppressWarnings("unused")
//			Entity tokenEntity = datastore.get(tokenKey);
//			LOG.info("Got token...");
//			if (token.expirationData <= System.currentTimeMillis()) {
//				LOG.warning("The received token has already expired!");
//				return Response.status(Status.FORBIDDEN).build();
//			}
//			else {
//				//register report
//				LOG.info("Registering report...");
//				EmbeddedEntity address = new EmbeddedEntity();
//				address.setProperty("street", report.address.street);
//				address.setProperty("complementary", report.address.complementary);
//				address.setProperty("city", report.address.city);
//				address.setProperty("cp", report.address.CP);
//				
//				Entity r = new Entity("Report", tokenParent);
//				r.setProperty("type", report.type);
//				r.setProperty("urgency", report.urgency);
//				r.setProperty("isPublic", report.isPublic);
//				r.setProperty("address", address);
//				r.setProperty("additional", report.additional);
//				r.setProperty("image", report.imageUrl);
//				r.setProperty("username", token.user);
//				datastore.put(r);
//				LOG.info("Report registered.");
//				return Response.ok(g.toJson(report)).build();
//			}
//		} catch (EntityNotFoundException e1) {
//			LOG.warning("The received token has no match in the database!");
//			return Response.status(Status.FORBIDDEN).build();
//		}
//		
//		//ReportData report = g.fromJson(obj.get("report"), ReportData.class);
//		/**JsonElement je = g.fromJson(json, JsonElement.class);
//		ReportData report = g.fromJson(je., ReportData.class);**/
//		
//		/**JsonElement jelement = new JsonParser().parse(json);
//		JsonObject  jobject = jelement.getAsJsonObject();
//	    jobject = jobject.getAsJsonObject("report");
//	    ReportData report = g.fromJson(jobject.toString(), ReportData.class);
//		
//		return Response.ok(g.toJson(report)).build();**/
//	}
	
	
	
	@POST
	@Path("/report")
	public Response registerReport(String json) {
		LOG.info("Attempting to register report...");
		
		// Extract report and token from main JSON Object
		JsonElement jelement = new JsonParser().parse(json);
		JsonObject  jobject = jelement.getAsJsonObject();
		JsonObject jreport = jobject.getAsJsonObject("report");
		JsonObject jtoken = jobject.getAsJsonObject("token");
	    ReportData data = g.fromJson(jreport.toString(), ReportData.class);
	    AuthToken token = g.fromJson(jtoken.toString(), AuthToken.class);
	    
	    // User key
	    Key tokenParent = KeyFactory.createKey("User", token.user);
	    // Token key
		Key tokenKey = KeyFactory.createKey(tokenParent, "Token", token.tokenID);
		
		// Validate token
		try {
			@SuppressWarnings("unused")
			Entity tokenEntity = datastore.get(tokenKey);	// If the entity does not exist an Exception is thrown. Otherwise,
			
			// Check if token is expired
			if (token.expirationData <= System.currentTimeMillis()) {
				LOG.warning("The received token has already expired!");
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (EntityNotFoundException e) {
			LOG.warning("The received token does not exist!");
			return Response.status(Status.FORBIDDEN).build();
		}
		
		// If token is valid,
		Transaction txn = datastore.beginTransaction();
		try {
			@SuppressWarnings("unused")
			Entity user = datastore.get(tokenParent);	// If the entity does not exist an Exception is thrown. Otherwise,
			
			Entity report = new Entity("Report", tokenParent);
			
			report.setIndexedProperty("type", data.type);
			report.setIndexedProperty("urgency", data.urgency);
			report.setIndexedProperty("isPublic", data.isPublic);
			report.setIndexedProperty("formattedAddress", data.formattedAddress);
			report.setIndexedProperty("description", data.description);
			report.setIndexedProperty("imageUrl", data.imageUrl);
			
			datastore.put(txn, report);
			txn.commit();
			LOG.info("Report stored successfully!");
			
			return Response.ok(g.toJson(data)).build();
		} catch (EntityNotFoundException e) {
			LOG.warning("The user was not found. Aborting operation...");
			txn.rollback();
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive())
				txn.rollback();
		}
	}
	
	
//	@POST
//	@Path("/myReports")
//	public Response getMyReports(AuthToken token) {
//		Key tokenParent = KeyFactory.createKey("User", token.user);
//		Key tokenKey = KeyFactory.createKey(tokenParent, "Token", token.tokenID);
//		
//		try {
//			@SuppressWarnings("unused")
//			Entity tokenEntity = datastore.get(tokenKey);
//			if (token.expirationData <= System.currentTimeMillis()) {
//				LOG.warning("The received token has already expired!");
//				return Response.status(Status.FORBIDDEN).build();
//			}
//			else {
//				List <ReportData> reports = new ArrayList<ReportData>();
//				Query q = new Query("Report").setAncestor(tokenParent);
//				List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
//				for(Entity e: results) {
//					EmbeddedEntity a = new EmbeddedEntity();
//					Address address = new Address(
//							(String) a.getProperty("street"),
//							(String) a.getProperty("complementary"),
//							(String) a.getProperty("city"),
//							(String) a.getProperty("CP")
//					);
//					
//					reports.add(new ReportData(
//						(String)e.getProperty("type"),
//						(String)e.getProperty("urgency"),
//						(boolean)e.getProperty("isPublic"),
//						address,
//						(String)e.getProperty("additional"),
//						(String)e.getProperty("imageUrl"),
//						(String)e.getProperty("username")
//		
//					)); 
//				}
//				
//				return Response.ok(g.toJson(reports)).build();
//			}
//		} catch (EntityNotFoundException e1) {
//			LOG.warning("The received token has no match in the database!");
//			return Response.status(Status.FORBIDDEN).build();
//		}
//	}

}

package pt.unl.fct.di.apdc.demo.util;

public class ReportData {
	
//	public String type;
//	public String urgency;
//	public boolean isPublic;
//	public Address address;
//	public String additional;
//	public String imageUrl;
//	public String username;
	
	public String type;
	public int urgency;
	public boolean isPublic;
	public String formattedAddress;
	public String description;
	public String imageUrl;
	
	
	public ReportData() {
		
	}
	
	public ReportData(String type, int urgency, boolean isPublic, String formattedAddress, String description, String imageUrl) {
		this.type = type;
		this.urgency = urgency;
		this.isPublic = isPublic;
		this.formattedAddress = formattedAddress;
		this.description = description;
		this.imageUrl = imageUrl;
	}

//	public ReportData(String type, String urgency, boolean isPublic,
//			Address address, String additional, String imageUrl, String username) {
//		this.type = type;
//		this.urgency = urgency;
//		this.isPublic = isPublic;
//		this.address = address;
//		this.additional = additional;
//		this.imageUrl = imageUrl;
//		this.username = username;
//	}
	
	
	

}

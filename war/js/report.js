var map;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat:  38.659784, lng:  -9.202765},
        zoom: 16
    });

    map.addEventListener('click', function(event) {
        // GET COORDS
    });
}

function submitReport(event) {
    var report = $('form[name="report"]').jsonify();
    report.imageUrl = "http://www.google.pt/";
    report.isPublic = report.isPublic === 'on' ? true : false;    // SEMPRE ON , CORRIGIR BUG
    report.urgency = Number(report.urgency);
    console.log(report);

    var token = localStorage.getItem('token');
    token = JSON.parse(token);
    console.log(token);

    $.ajax({
        type: "POST",
        url: "../rest/operation/report",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        success: function(response) {
            updateReportList(response);
        },
        error: function(response) {
            alert("Oops! Something went wrong...");
        },
        data: JSON.stringify({
            report: report,
            token: token
        })
    });

    event.preventDefault();
}

function updateReportList(report) {
    $('.list-group').append(function() {
        console.log(report);
        return '<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">' + '<div class="d-flex w-100 justify-content-between">' + '<h5 class="mb-1">Report</h5>' + '</div>' + '<p class="mb-1">' + '<strong>' + 'type: ' + '</strong>' + report.type + '</p>' + '<p class="mb-1">' + '<strong>' + 'urgency: ' + '</strong>' + report.urgency + '</p>' + '<p class="mb-1">' + '<strong>' + 'address: ' + '</strong>' + report.formattedAddress + '</p>' + '<p class="mb-1">' + '<strong>' + 'description:<br> ' + '</strong>' + report.description + '</p>' + '</a>';



            /**'<div class="d-flex w-100 justify-content-between">' +
                '<h5 class="mb-1">Report</h5><br>' +
            '</div>';**/
            /**
            '<span class="mb-1"><strong>Type:</strong> asdasdadsasdas' + report.type + '</span><br>' +
            '<span class="mb-1"><strong>Urgency:</strong> ' + report.urgency + '</span><br>' +
            '<span class="mb-1"><strong>Address:</strong> ' + report.formatedAddress + '</span><br>' +
            '<p class="mb-1">' + report.description + '</p>' +
            **/
    });
}

window.onload = function() {
    var frms = $('form');
    frms[0].onsubmit = submitReport;
}
